export interface UserPayloadInterface {
    email: string;
    password: string;
}

export interface INotice {
    title: string
    text: string
    // img: string
}

export interface IRiderList {
  id: number
  name: string
  email: string
  dateJoin: string
  phoneNumber: string
  driveType: string
  addressWish: string
  admin: string
}

export interface INAdmin {
  name: string
  email: string
}
