import {defineStore} from 'pinia';
import type {INotice} from "~/interface";


// board 스토어
export const useNoticeStore = defineStore('board', {
    state: () => ({
        boards:[],
    }),
    actions: {
        setBoard(data: INotice) {
            useFetch('http://matddak.shop:8080/v1/board/new', {
                method: 'POST',
                body: data
            })
        },
        async fetchNotice(boardId:number) {
            const token = useCookie('token');
            const { id} = useRoute().params
            const {data}:any = await useFetch(
                `http://matddak.shop:8080/v1/board/detail/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            console.log(data);
            if (data.value) {
                this.boards = data.value;
            }
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useNoticeStore, import.meta.hot))
}
