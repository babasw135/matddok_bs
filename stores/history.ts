import {defineStore} from 'pinia';

// auth 스토어
export const useHistoryStore = defineStore('history', {
  state: () => ({
    moneyList: [],
    money: {},
  }),
  actions: {
    async fetchHistoryList() {
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/money-history/all/out/1`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        }
      );
      if (data.value) {
        this.moneyList = data.value.list;
        console.log(data);
      }
    },
    async fetchHistory(historyId: number) {
      const token=useCookie('token');
      const {id} = useRoute().params;
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/money-history/detail/${id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        }
      );
      if (data) {
        this.money=data.value.data;
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useHistoryStore, import.meta.hot))
}
