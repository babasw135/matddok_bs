import {acceptHMRUpdate, defineStore} from 'pinia';

interface riderList {
  id: number
  name: string
  email: string
  dateJoin: string
  phoneNumber: string
  driveType: string
  addressWish: string
  admin: string
}


// rider 스토어
export const useRiderStore = defineStore('tableData', {
    state: () => ({
      tableData: [] as riderList[],
    }),
    actions: {
        async fetchRider() {
            // const token = useCookie('token');
            const { data }:any = await useFetch(
                `http://matddak.shop:8080/v1/rider/all`, {
                    method: 'GET',
                    // headers: {
                    //     'Authorization': `Bearer ${token}`
                    // },
                });
            if (data) {
                this.tableData = data.value.list;
            }
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useRiderStore, import.meta.hot))
}
