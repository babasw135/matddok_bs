import {defineStore} from 'pinia';
import type {UserPayloadInterface} from "~/interface";


export const useAuthStore = defineStore('auth', {
    state: () => ({
        authenticated: false,
        loading: false,
    }),
    actions: {
        async authenticateUser({email, password}: UserPayloadInterface) {
            const {data}: any = await useFetch('http://matddak.shop:8080/v1/login/web/admin', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: {
                    email,
                    password,
                },
            });
            console.log(data)
            if (data.value) {
                const token = useCookie('token');
                token.value = data?.value?.data?.token;
                this.authenticated = true;
            }
        },
        logUserOut() {
            const token = useCookie('token');
            this.authenticated = false;
            token.value = null;
        },
    },
});
